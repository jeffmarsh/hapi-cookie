/**
 * Created by jeff_marsh on 9/25/14.
 */
var Hapi = require('hapi');
var assert = require('assert');

describe('Hapi server', function() {
	var server = null;

	beforeEach(function() {
		server = new Hapi.Server();
	});

	afterEach(function () {
		server = null;
	});

	it('should be able to register the plugin', function(done) {
		server.pack.register({
			plugin: require('../'),
			options: {}
		}, function (err) {
			assert(err === undefined, 'An error was thrown but should not have been.');
			done();
		});
	});

	it('should be able to find the plugin exposed method(s)', function(done) {
		server.pack.register({
			plugin: require('../'),
			options: {}
		}, function (err) {
			server.route({ method: 'GET', path: '/', handler: function(request, reply) {
				var hapiCookie = request.server.plugins['hapi-cookie'];
				assert(hapiCookie.setCookie, 'Could not find setCookie method');
				done();
			}});

			server.inject({ method: 'GET', url: '/' }, function() {});
		});
	});

	it('should be able to set a cookie', function(done) {
		server.pack.register({
			plugin: require('../'),
			options: {}
		}, function (err) {
			server.route({ method: 'GET', path: '/', handler: function(request, reply) {
				var hapiCookie = request.server.plugins['hapi-cookie'];
				var dur = 100000000; // add some tiem so the cookie doesn't expire right away
				var dte = new Date(new Date().getTime()+dur).toUTCString();

				hapiCookie.setCookie("testCookie", "cookieValue", dte, "myDomain.com", "/", true, true, function(e, cookieVal){
					if (e || cookieVal == null){throw e}
				});
				done();
			}});

			server.inject({ method: 'GET', url: '/' }, function() {});
		});
	});
});