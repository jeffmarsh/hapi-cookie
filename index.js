/**
 * Created by jeff_marsh on 8/6/14.
 */

var _ = require('lodash');

exports.register = function(plugin, options, next) {

    var cookiesToAdd = [];

    plugin.expose('setCookie', function(name, value, expires, path, domain, isSecure, isHttpOnly, cb){
        if (name){
            var strA = [];
            strA.push(name+"="+value);
            if (expires){
                strA.push("expires="+expires);
            }
            if (path){
                strA.push("path="+path);
            }
            if (domain){
                strA.push("domain="+domain);

            }
            if (typeof(isSecure) == "boolean"){

                strA.push(isSecure.toString());

            }
            if (typeof(isHttpOnly) == "boolean"){

                strA.push(isHttpOnly.toString());

            }
            var cookie = strA.join("; ");
            cookiesToAdd.push(cookie);
            cb(null, cookie);
        } else {
            cb("No Name was present. Cookie Not added", null);
        }

    });
    plugin.ext('onPreResponse', function (request, extNext) {

        if (cookiesToAdd.length > 0){
            for (var cnt=0; cnt < cookiesToAdd.length; cnt++){
                //console.log("Adding Cookie: "+cookiesToAdd[cnt]);
                _.extend(request.response.headers, { 'Set-Cookie' : cookiesToAdd[cnt]} );
            }
            //clear the array
            cookiesToAdd.length =0;
        }
        extNext();
    });

    next();
};

exports.register.attributes = {
	pkg: require('./package.json')
};